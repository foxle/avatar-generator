﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace FoxleAvi
{
    public enum FoxleBackground
    {
        Transparent,
        Grass,
    }

    public class Foxle
    {

        static readonly string[] bases = {
            "classic",
            "coffee",
            "dawnbringer",
            "flame",
            "frost",
            "watermelon",
        };

        static readonly Image grass = Image.FromFile(Path.Join("Images", "grass.png"));

        static readonly MD5 md5 = MD5.Create();

        readonly byte[] hash;

        public Foxle(string data)
        {
            hash = md5.ComputeHash(Encoding.UTF8.GetBytes(data));
        }

        public byte[] Generate(int padding = 0, bool crop = false)
        {
            // Create canvas
            using var image = new Bitmap(160, 160);
            using var graphics = Graphics.FromImage(image);
            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
            graphics.DrawImage(grass, 0, 0, 160, 160);

            // Fox
            int size = 160 - (crop ? 0 : padding * 2);
            graphics.DrawImage(BaseFoxle, crop ? 48 : padding, crop ? 32 : padding, size, size);

            if (UseFlip)
            {
                image.RotateFlip(RotateFlipType.RotateNoneFlipX);
            }

            // Export to PNG
            using var memory = new MemoryStream();
            image.Save(memory, ImageFormat.Png);
            return memory.ToArray();
        }

        Image BaseFoxle
        {
            get
            {
                string color = bases[BitConverter.ToUInt16(hash, 0) % bases.Length];
                int frame = hash[1] > 0xAF ? 8 : 0;

                return Image.FromFile(Path.Join("Images", $"foxle_{color}_{frame}.png"));
            }
        }

        bool UseFlip => hash[3] > 0x7F;
    }
}
