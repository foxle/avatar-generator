﻿using System.Net;

namespace FoxleAvi
{
	class Program
	{
        static HttpListener httpListener;

        static readonly byte[] errorResponse = System.Text.Encoding.UTF8.GetBytes("Missing query parameter: data\nOptional: padding (0-80px) or crop (0 for full or 1 for face)");

        static void Main(string[] args) {
            var prefix = args.Length >= 2 ? args[1] : "127.0.0.1:8403";

			httpListener = new HttpListener();
            httpListener.Prefixes.Add($"http://{prefix}/");
            httpListener.Start();

            while (httpListener.IsListening) {
                var context = httpListener.GetContext();
                var data = context.Request.QueryString.Get("data");
                bool crop = context.Request.QueryString.Get("crop") == "1";
                int padding;

                if (int.TryParse(context.Request.QueryString.Get("padding"), out int p)) {
                    padding = System.Math.Clamp(p, 0, 80);
                } else {
                    padding = 0;
                }

                var response = context.Response;

                if (data == null) {
                    response.StatusCode = 400;
                    response.ContentType = "text/plain";
                    response.OutputStream.Write(errorResponse, 0, errorResponse.Length);
                    response.OutputStream.Close();
                } else {
                    var foxle = new Foxle(data).Generate(padding, crop);
                    response.StatusCode = 200;
                    response.ContentType = "image/png";
                    response.ContentLength64 = foxle.Length;
                    response.OutputStream.Write(foxle, 0, foxle.Length);
                    response.OutputStream.Close();
                }
            }

            httpListener.Stop();
        }
	}
}
